<b>Custom SeekBar</b>

Here are some attributes that you can use to adjust the Seekbar.

<b>Track attributes:</b>

Change track color, track progress color, track width, track corner radius, min value and max value of the seekbar.

You can set attributes values from XML as following:
```Android
app:track_color="@color/colorAccent"
app:track_progress_color="@color/colorPrimary"
app:track_width="200dp"
app:track_corner_radius="5dp"
```
Or you can set the values within java code as following:
```
mSeekBar.setMin(50);
mSeekBar.setMax(100);
mSeekBar.setProgressTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
mSeekBar.setProgressTrackColor(ContextCompat.getColor(this, R.color.colorPrimary));
mSeekBar.setTrackColor(ContextCompat.getColor(this, R.color.colorPrimary));
mSeekBar.setTrackCornerRadius(10);
```

<b>Thumb attributes:</b>

Change thumb color, thumb radius and progress text color

You can set attributes values from XML file as following:
```android
app:thumb_progress_text_color="@color/colorAccent"
app:thumb_color="@color/colorPrimary"
```
Or you can set the values within java code as following:
```
mSeekBar.setThumbColor(ContextCompat.getColor(this, R.color.colorPrimary));
mSeekBar.setThumbAndProgressTrackColor(ContextCompat.getColor(this, R.color.colorPrimary));
mSeekBar.setThumbRadius(ContextCompat.getColor(this, R.color.colorPrimary));
```
<b>Max Value text attributes:</b>

You can change the max value text color as following
```android
app:max_value_text_color="@color/colorAccent"
```

<b>Screenshot:</b>

<a href="https://ibb.co/dyQLa7"><img src="https://preview.ibb.co/knNNoS/custom_seekbar.png" alt="custom_seekbar" border="0"></a>
