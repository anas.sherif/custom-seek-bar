package com.anas.customseekbar.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.anas.customseekbar.OnVodafoneSeekBarChangeListener;
import com.anas.customseekbar.R;
import com.anas.customseekbar.utils.DimentionsUtil;

/**
 * Created by Anas on 3/1/2018.
 */

public class VodafoneSeekBar extends View {

    private OnVodafoneSeekBarChangeListener mOnVodafoneSeekBarChangeListener;
    private Paint mPaint;
    private RectF mRectF;
    private Rect textBounds;
    private TypedArray typedArray;

    // Track attrs
    private int mTrackColor;
    private int mProgressTrackColor;
    private int mTrackHeight;
    private int mTrackWidth;
    private int mTrackCornerRadius;
    // Thumb attrs
    private int mThumbColor;
    private int mThumbProgressTextColor;
    private int mThumbProgressTextSize;
    private int mThumbRadius;
    // Max value attrs
    private String mMaxValue;
    private int mMaxValueTextColor;
    private int mMaxValueTextSize;
    private int mMaxValueTextWidth;
    private int mMaxValueTextMarginLeft;

    private float mTouchX;
    private int mMin = 0;
    private int mMax = 100;
    private String mMaxString;
    private int mProgress;
    private String mProgressString;
    private int mSeekLength;
    private int mMeasuredWidth;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int textAlignYPosition;
    private boolean mDrawBackground = false;

    public VodafoneSeekBar(Context context) {
        super(context);
        init(null);
    }

    public VodafoneSeekBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public VodafoneSeekBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public VodafoneSeekBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPaint(mPaint);

        drawMaxValueText(canvas);
        drawTrack(canvas);
        drawProgressTrack(canvas);
        drawThumb(canvas);
        if (mDrawBackground) {
            drawBackgroundColor();
        } else {
            drawTransparentBackgroundColor();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        calculateMaxValueTextWidth();
        setMeasuredDimension(mTrackWidth + mThumbRadius + mMaxValueTextMarginLeft + mMaxValueTextWidth, mThumbRadius * 2);
        initSeekBarInfo();
    }

    private void init(@Nullable AttributeSet attrs) {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRectF = new RectF();
        textBounds = new Rect();
        typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.VodafoneSeekBar);
        initPaint();
        initTrackAttrs();
        initThumbAttrs();
        initMaxValueTextAttrs();
    }

    private void initPaint() {
        mPaint.setColor(ContextCompat.getColor(getContext(), R.color.gray));
        mPaint.setStyle(Paint.Style.FILL);
    }

    private void initTrackAttrs() {
        mTrackHeight = (int) getResources().getDimension(R.dimen.track_height);
        mDrawBackground = typedArray.getBoolean(R.styleable.VodafoneSeekBar_draw_background, false);
        mTrackColor = typedArray.getColor(R.styleable.VodafoneSeekBar_track_color,
                ContextCompat.getColor(getContext(), R.color.dark_blue));
        mProgressTrackColor = typedArray.getColor(R.styleable.VodafoneSeekBar_track_progress_color, Color.RED);
        mTrackWidth = typedArray.getDimensionPixelSize(R.styleable.VodafoneSeekBar_track_width,
                (int) getResources().getDimension(R.dimen.track_width));
        mTrackCornerRadius = typedArray.getDimensionPixelSize(R.styleable.VodafoneSeekBar_track_corner_radius,
                (int) getResources().getDimension(R.dimen.track_corner_radius));
    }

    private void initThumbAttrs() {
        mThumbRadius = getResources().getDimensionPixelOffset(R.dimen.thumb_radius);
        mThumbColor = typedArray.getColor(R.styleable.VodafoneSeekBar_thumb_color, Color.RED);
        mThumbProgressTextSize =  getResources().getDimensionPixelOffset(R.dimen.thumb_text_size);
        mThumbProgressTextColor = typedArray.getColor(R.styleable.VodafoneSeekBar_thumb_progress_text_color, Color.WHITE);


    }

    private void initMaxValueTextAttrs() {
        mMaxValueTextColor = typedArray.getColor(R.styleable.VodafoneSeekBar_max_value_text_color,
                ContextCompat.getColor(getContext(), R.color.dark_blue));
        mMaxValueTextSize = getResources().getDimensionPixelOffset(R.dimen.max_value_text_size);

        typedArray.recycle();
    }

    private void initSeekBarInfo() {
        mMeasuredWidth = getMeasuredWidth() - mThumbRadius * 2 - mMaxValueTextWidth - mMaxValueTextMarginLeft;
        mPaddingLeft = getPaddingLeft();
        mPaddingRight = getPaddingRight();

        mSeekLength = mMeasuredWidth - mPaddingLeft - mPaddingRight;
        mMaxValueTextMarginLeft = mThumbRadius * 4;
        mMaxValue = mMaxString;
        calculateProgress();
    }

    private float adjustTouchX(MotionEvent event) {
        float mTouchXCache;
        if (event.getX() < mPaddingLeft) {
            mTouchXCache = mPaddingLeft;
        } else if (event.getX() > mMeasuredWidth - mPaddingRight) {
            mTouchXCache = mMeasuredWidth - mPaddingRight;
        } else {
            mTouchXCache = event.getX();
        }
        return mTouchXCache;
    }

    private void calculateTouchX(float touchX) {
        mTouchX = touchX + mPaddingLeft;

    }

    private void updateSeekBar(MotionEvent event) {
        calculateTouchX(adjustTouchX(event));
        calculateProgress();
        invalidate();
    }

    private void calculateProgress() {
        mProgress = (int) (mMin + (mMax - mMin) * (mTouchX - mPaddingRight) / mSeekLength);
        mProgressString = String.valueOf(mProgress);
    }


    private void drawTrack(Canvas canvas) {
        mRectF.left = mThumbRadius;
        mRectF.top = mThumbRadius / 2;
        mRectF.right = mTrackWidth + mThumbRadius;
        mRectF.bottom = mTrackHeight + mThumbRadius / 2;

        mPaint.setColor(mTrackColor);
        canvas.drawRoundRect(mRectF, mTrackCornerRadius, mTrackCornerRadius, mPaint);
    }

    private void drawProgressTrack(Canvas canvas) {

        mRectF.left = mThumbRadius;
        mRectF.top = mThumbRadius / 2;
        mRectF.right = mMin + mTouchX - mMin + mThumbRadius;
        mRectF.bottom = mTrackHeight + mThumbRadius / 2;

        mPaint.setColor(mProgressTrackColor);
        canvas.drawRoundRect(mRectF, mTrackCornerRadius, mTrackCornerRadius, mPaint);
    }

    private void drawThumb(Canvas canvas) {
        // draw thumb circle
        mPaint.setColor(mThumbColor);
        canvas.drawCircle(mTouchX + mThumbRadius, mThumbRadius, mThumbRadius, mPaint);
        // draw thumb text
        mPaint.setColor(mThumbProgressTextColor);
        mPaint.setTextSize(mThumbProgressTextSize);
        mPaint.setTextAlign(Paint.Align.CENTER);
        int yPos = (int) ((canvas.getHeight() / 2) - ((mPaint.descent() + mPaint.ascent()) / 2));

        canvas.drawText(mProgressString, mTouchX + mThumbRadius, yPos, mPaint);
    }

    private void drawMaxValueText(Canvas canvas) {
        mPaint.setColor(mMaxValueTextColor);
        mPaint.setTextSize(mMaxValueTextSize);
        mPaint.setTextAlign(Paint.Align.CENTER);
        textAlignYPosition = (int) ((canvas.getHeight() / 2) - ((mPaint.descent() + mPaint.ascent()) / 2));

        canvas.drawText(mMaxValue, mTrackWidth + mMaxValueTextMarginLeft, textAlignYPosition, mPaint);
    }

    private void drawBackgroundColor() {
        mPaint.setColor(ContextCompat.getColor(getContext(), R.color.gray));
    }

    private void drawTransparentBackgroundColor() {
        mPaint.setColor(Color.TRANSPARENT);
    }

    public void setOnVodafoneSeekBarChangeListener(OnVodafoneSeekBarChangeListener onVodafoneSeekBarChangeListener) {
        this.mOnVodafoneSeekBarChangeListener = onVodafoneSeekBarChangeListener;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mOnVodafoneSeekBarChangeListener != null) {
                    mOnVodafoneSeekBarChangeListener.onStartTrackingTouch(this);
                }
                updateSeekBar(event);
                break;
            case MotionEvent.ACTION_MOVE:
                updateSeekBar(event);
                if (mOnVodafoneSeekBarChangeListener != null) {
                    mOnVodafoneSeekBarChangeListener.onProgressChanged(this, mProgress);
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mOnVodafoneSeekBarChangeListener != null) {
                    mOnVodafoneSeekBarChangeListener.onStopTrackingTouch(this);
                }
                break;
        }
        return true;
    }

    private void calculateMaxValueTextWidth() {
        mMaxString = String.valueOf(mMax);
        mPaint.getTextBounds(String.valueOf(mMax), 0, mMaxString.length(), textBounds);
        mMaxValueTextWidth = textBounds.width();
        mMaxValueTextWidth = DimentionsUtil.convertPixelsToDp(mMaxValueTextWidth, getContext());
    }

    private int getMaxValueTextWidth() {
        return mMaxValueTextWidth;
    }

    // Helper functions
    public void setMax(int max) {
        this.mMax = max;
        this.mMaxValue = String.valueOf(max);
        invalidate();
    }

    public int getMax() {
        return mMax;
    }

    public void setMin(int min) {
        this.mMin = min;
    }

    public int getMin() {
        return mMin;
    }

    public void setProgressValue(int progress) {
        this.mProgress = progress;
    }

    public int getProgressValue() {
        return mProgress;
    }

    public void setThumbColor(int thumbColor) {
        this.mThumbColor = thumbColor;
    }

    public void setTrackColor(int trackColor){
        this.mTrackColor = trackColor;
    }
    public void setProgressTrackColor(int trackProgressColor) {
        this.mProgressTrackColor = trackProgressColor;
    }

    public void setThumbAndProgressTrackColor(int color) {
        this.mThumbColor = color;
        this.mProgressTrackColor = color;
    }

    public void setTrackCornerRadius(int radius){
        this.mTrackCornerRadius = radius;
    }
    public void setProgressTextColor(int color) {
        this.mThumbProgressTextColor = color;
    }

    public void setThumbRadius(int radius) {
        this.mThumbRadius = radius;
    }

}
