package com.anas.customseekbar;

import android.widget.SeekBar;

import com.anas.customseekbar.views.VodafoneSeekBar;

/**
 * Created by Anas on 3/1/2018.
 */

public interface OnVodafoneSeekBarChangeListener {
    void onProgressChanged(VodafoneSeekBar vodafoneSeekBar, int progress);
    void onStartTrackingTouch(VodafoneSeekBar vodafoneSeekBar);
    void onStopTrackingTouch(VodafoneSeekBar vodafoneSeekBar);
}
