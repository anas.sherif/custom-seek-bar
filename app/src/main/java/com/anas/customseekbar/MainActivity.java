package com.anas.customseekbar;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.anas.customseekbar.views.VodafoneSeekBar;

public class MainActivity extends AppCompatActivity implements OnVodafoneSeekBarChangeListener {


    private VodafoneSeekBar mVodafoneSeekBar;
    private VodafoneSeekBar mVodafoneSeekBar2;

    private EditText mUpdateMaxEditText;
    private Button umUpdateMaxBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        mVodafoneSeekBar.setOnVodafoneSeekBarChangeListener(this);
        mVodafoneSeekBar2.setOnVodafoneSeekBarChangeListener(this);

        // Update Max value with button click
        umUpdateMaxBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mUpdateMaxEditText.getText().toString().isEmpty()) {
                    int progress = Integer.parseInt(mUpdateMaxEditText.getText().toString());
                    mVodafoneSeekBar2.setMax(progress);
                }
            }
        });

    }

    private void initViews() {
        umUpdateMaxBtn = findViewById(R.id.updateMaxSeekBar2);
        mUpdateMaxEditText = findViewById(R.id.updateMaxEditText);
        mVodafoneSeekBar = findViewById(R.id.seekbar);
        mVodafoneSeekBar2 = findViewById(R.id.seekbar2);
    }

    @Override
    public void onProgressChanged(VodafoneSeekBar vodafoneSeekBar, int progress) {

        switch (vodafoneSeekBar.getId()) {
            case R.id.seekbar:
//                Log.e("Seek 1", String.valueOf(progress));
                break;
            case R.id.seekbar2:
//                Log.e("Seek 2", String.valueOf(progress));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(VodafoneSeekBar vodafoneSeekBar) {

    }

    @Override
    public void onStopTrackingTouch(VodafoneSeekBar vodafoneSeekBar) {

    }

}
